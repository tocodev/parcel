/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var Queue = require('../../models/queue');
var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');
var jwt = require('jsonwebtoken');
const moment = require('moment');

// Comes in handy for mocks that return promises
function makeEmptyPromise() {
    return new Promise(function (fulfill){fulfill();});
}

describe('QueueModel', function(){

    mochaGen.install();

    var sandbox;
    var jwtSignMock;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
        // Mock the JWT sign to always return hardcoded token
        jwtSignMock = sandbox.stub(jwt, 'sign', function(payload, secret) {
            return "IAMANIDIOT";
        });
    })

    afterEach(function(){
        sandbox.restore();
    });



    it('should be invalid if name is empty', function(done){
        let scope = new Queue({description: 'Blah blah'});

        scope.validate(function(err) {
            expect(err.errors.name).to.be.defined;
            done();
        });
    });

    it('should default some values upon creation', function(){
        let sut = new Queue();
        expect(sut.security).to.equal("none");
        expect(sut.method).to.equal("FIFO");
    });

    it('should return a generated token when generateToken is called', function(){
        var sut = new Queue();
        sut.generateToken();

        sinon.assert.calledOnce(jwtSignMock);
        expect(sut.token).to.equal("IAMANIDIOT");
    });

    it('should do nothing when validateToken is called (since it has no functionality yet)', function(){
    });

    it("should call mongoose findOne on load", function(){
        sandbox.stub(Queue, 'findOne', function(){return -1});
        var result = Queue.load("name", {});
        expect(result).to.equal(-1); // cheap 'hasBeenCalled' assert
    });

    it("should call mongoose findOneAndRemove on delete", function*(){
        sandbox.stub(Queue, 'findOneAndRemove', function(){return -1});
        var result = yield Queue.delete("name");
        expect(result).to.equal(-1); // cheap 'hasBeenCalled' assert
    });

    it("should validate a queue token", function(){
        let queue = new Queue({name:'TestQueue'});
        let token = null;
        var result = queue.validateToken(token);
    });
});