/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
var Consumer = require('../../models/consumer');
var Queue = require('../../models/queue');
var config = require('../../config');
var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');

describe('Consumer Model', function () {

    mochaGen.install();

    var sandbox;

    beforeEach(function () {
        sandbox = sinon.sandbox.create();
    })

    afterEach(function () {
        sandbox.restore();
    });

    // Comes in handy for mocks that return promises
    function makeEmptyPromise() {
        return new Promise(function (fulfill) {
            fulfill();
        });
    }

    it('should be invalid if no endpoint is given', function (done) {
        var sub = new Consumer({name: 'yayayaya'});

        sub.validate(function (err) {
            expect(err.errors.endpoint).to.be.defined;
            done();
        })
    });

    it('should be invalid if no name is given', function (done) {
        var sub = new Consumer({subscription: {endpoint: 'http://hello.com/1234'}});

        sub.validate(function (err) {
            expect(err.errors.name).to.be.defined;
            done();
        })
    });

    it("should handle a new consumer on a fresh, insecure queue", function*() {
        var queue = new Queue({
            name: 'TestQueue',
            id: new mongoose.Types.ObjectId(),
            security: config.queueSecurity.none
        });

        var consumer = new Consumer({
            name: 'TestConsumer',
            subscription: {
                url: 'http://test.com/testme'
            }
        });

        sandbox.stub(Consumer, 'findOne', function () {
            return makeEmptyPromise()
        });

        // Create mock for this.model().findOne() to return nothing (promise)
        let fakeModel = makeEmptyPromise();
        fakeModel.findOne = function (whatever) {
            return makeEmptyPromise();
        }
        sandbox.stub(consumer, 'save', function*() {
            this._id = mongoose.Types.ObjectId();
            return this;
        });

        let savedConsumer = yield Consumer.consume(queue, consumer);

        expect(savedConsumer._id).to.be.defined;
        expect(savedConsumer.token).to.not.exist;
    });

    it("should blow up when re-adding consumer", function*() {
        var queue = new Queue();
        queue._id = mongoose.Types.ObjectId();

        var consumer = new Consumer({
            queueId: queue._id,
            name: 'TestConsumer',
            subscription: {
                url: 'http://test.com/testme'
            }
        });

        var existing = new Consumer({
            queueId: queue._id,
            name: 'TestConsumer',
            subscription: {
                url: 'http://test.com/testme'
            }
        });

        sandbox.stub(Consumer, 'findOne', function*() {
            return existing
        });

        let savedConsumer = null;
        try {
            savedConsumer = yield Consumer.consume(queue, consumer);
        } catch (ex) {
            expect(ex).to.equal(config.errors.subscriptionExists);
        }

        expect(savedConsumer).to.not.exist;
    });

    it("should handle a new consumer on a fresh, secure queue", function*() {
        var queue = new Queue({
            _id: mongoose.Types.ObjectId(),
            security: config.queueSecurity.token,
            token: 'notARealToken'
        });

        var consumer = new Consumer({
            queueId: queue._id,
            name: 'TestConsumer',
            subscription: {
                url: 'http://test.com/testme'
            }
        });

        sandbox.stub(Consumer, 'findOne', function () {
            return makeEmptyPromise()
        });

        // Create mock for this.model().findOne() to return nothing (promise)
        let fakeModel = makeEmptyPromise();
        fakeModel.findOne = function (whatever) {
            return makeEmptyPromise();
        }
        sandbox.stub(consumer, 'save', function*() {
            this._id = mongoose.Types.ObjectId();
            return this;
        });

        let savedConsumer = yield Consumer.consume(queue, consumer);

        expect(savedConsumer._id).to.be.defined;
        expect(savedConsumer.token).to.be.defined;
    });

    it("should return nothing when quit consuming a consumer that isn't there", function*(){

        var queue = new Queue({
            _id: mongoose.Types.ObjectId(),
            security: config.queueSecurity.token
        });

        sandbox.stub(Consumer, 'findOneAndRemove', function () {
            return makeEmptyPromise()
        });

        let savedConsumer = yield Consumer.regurgitate(queue, 'TestSub');

        expect(savedConsumer).to.not.exist;
    });

    it("should return the removed consumer when quit consuming a consumer that is there", function*(){
        var queue = new Queue({
            _id: mongoose.Types.ObjectId(),
            security: config.queueSecurity.token
        });

        var consumer = new Consumer({
            queueId: queue._id,
            name: 'TestConsumer',
            endpoint: 'http://test.com/testme'
        });

        sandbox.stub(Consumer, 'findOneAndRemove', function* () {
            return consumer;
        });

        // Unsubscribe something that IS there
        let savedConsumer = yield Consumer.regurgitate(queue, consumer.name);
        expect(savedConsumer).to.exist;
        expect(savedConsumer.name).to.equal(consumer.name);
    });

    it('should require a queue parameter when consuming', function* (){
        let savedConsumer = null;
        var consumer = new Consumer({
            name: 'TestConsumer',
            endpoint: 'http://test.com/testme'
        });
        try {
            savedConsumer = yield Consumer.consume(null, consumer);
        } catch (ex) {
            expect(ex).to.equal(config.errors.queueRequired);
        }
        expect(savedConsumer).to.not.exist;
    });

    it('should require a queue parameter when quir consuming', function* (){
        let savedConsumer = null;
        try {
            savedConsumer = yield Consumer.regurgitate(null, 'SomeConsumer');
        } catch (ex) {
            expect(ex).to.equal(config.errors.queueRequired);
        }
        expect(savedConsumer).to.not.exist;
    });

    it('should return all consumers for a given queue', function* (){
        let queueId = mongoose.Types.ObjectId();
        let queue = new Queue({
            _id: queueId,
            name: 'TestQueue',
            security: config.queueSecurity.none
        });

        let allConsumers = [{
            queueId: queueId,
            name:'TestSub1',
            endpoint: 'http://test.com/sub1'
        },{
            queueId: queueId,
            name:'TestSub2',
            endpoint: 'http://test.com/sub2'
        }];

        sandbox.stub(Consumer, 'find', function*() {return allConsumers});

        let consumers = yield Consumer.loadAllForQueue(queue);

        expect(consumers).to.exist;
        expect(consumers.length).to.equal(2);
    });


});