/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
var Message = require('../../models/message');
var Queue = require('../../models/queue');

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

describe('Message Schema', function(){

    var sandbox;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
    })

    afterEach(function(){
        sandbox.restore();
    });

    it('should default some values upon creation', function(){
        var sut = new Message();
        expect(sut.createdDate).to.be.defined;
    });

    it('should findOne in db sorted asc when not given a method', function(){
        sandbox.stub(Message, 'findOne', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(1);
            }};
        });
        sandbox.stub(Message, 'findOneAndRemove', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(1);
            }};
        });

        Message.browse(123);
        Message.get(123);
    });

    it('should findOne in db sorted asc when given FIFO', function(){
        sandbox.stub(Message, 'findOne', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(1);
            }};
        });
        sandbox.stub(Message, 'findOneAndRemove', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(1);
            }};
        });

        Message.browse(123, "FIFO");
        Message.get(123, "FIFO");
    });

    it('should findOne in db sorted desc when given LIFO', function(){
        sandbox.stub(Message, 'findOne', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(-1);
            }};
        });
        sandbox.stub(Message, 'findOneAndRemove', function (queueDef){
            expect(queueDef.queueId).to.equals(123);
            return {sort: function(criteria) {
                expect(criteria.createdDate).to.equal(-1);
            }};
        });

        Message.browse(123, "LIFO");
        Message.get(123, "LIFO");
    });

    it('should call save on put - yeah not super useful test...', function(){
        let messageObj = {save: function(){}};
        let saveStub = sandbox.stub(messageObj, 'save', function (){
            return {};
        });

        Message.put(messageObj);
        sinon.assert.calledOnce(saveStub);
    });

    it('should call underlying count on depth - yeah not super useful test...', function(){
        let countStub = sandbox.stub(Message, 'count', function (queueDef){
            expect(queueDef.queueId).to.equal(100);
            return 3;
        });

        var queueDepth = Message.depth(100);
        expect(queueDepth).to.equal(3);
        sinon.assert.calledOnce(countStub);
    });
});