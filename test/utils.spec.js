/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var sinon = require('sinon');
var utils = require('../utils');
var config = require('../config');
var chai = require('chai');
var expect = chai.expect;
var Message = require('../models/message');
var request = require('request');

describe('Utils', function(){

    var sandbox;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
    })

    afterEach(function(){
        sandbox.restore();
    });

    it('should be able to extract parcel headers', function(){
        let headers = { };
        headers[config.headerPrefix+'-test'] = 'test content';

        let extractedHeaders = utils.extractHeaders(headers);

        expect(Object.keys(extractedHeaders).length).to.equal(1);
        expect(extractedHeaders[config.headerPrefix+'-test']).to.equal('test content');
    });

    it('should not extract non-parcel headers', function(){
        let headers = {
            'Content-Type': 'application/json',
            'Random-Header': 'SomeDataHere'
        };

        let extractedHeaders = utils.extractHeaders(headers);

        expect(Object.keys(extractedHeaders).length).to.equal(0);
        expect(extractedHeaders['Content-Type']).to.be.defined;
        expect(extractedHeaders['Random-Header']).to.not.be.defined;
    });

    it('should be able verify that an error is a parcel error', function(){
        let result = utils.isParcelError(config.errors.subscriptionExists);

        expect(result).to.equal(true);
    });

    it('should not detect other objects as parcel errors', function(){
        let result = utils.isParcelError({random: 'stuff'});

        expect(result).to.equal(false);
    });

    it('should handle broadcasting a message to a list of subscribers', function(){
        let payload = {data:'Some data'};
        let message = new Message({payload:payload});

        let con1 = {name: 'Consumer1', subscription: {url: 'http://sub1.com/msg'}};
        let con2 = {name: 'Consumer1', subscription: {url: 'http://sub2.com/msgs'}};

        let consumers = [ con1, con2 ];

        let sendMessageMock = sandbox.stub(utils, 'sendMessage', function(endpoint, message){
            expect(message.payload).to.be.defined;
            expect(message.payload.data).to.equal(payload.data);
        });

        utils.broadcastMessage(consumers, message);

        sinon.assert.calledTwice(sendMessageMock);
    });

    it('should do nothing if no subscriptions are passed', function(){
        let subscriptions = null;
        let message = new Message({payload: 'blah'});

        let sendMessageMock = sandbox.stub(utils, 'sendMessage', function(endpoint, message){
            expect(message.payload).to.be.defined;
            expect(message.payload.data).to.equal(payload.data);
        });

        utils.broadcastMessage(subscriptions, message);

        sinon.assert.notCalled(sendMessageMock);
    });

    it('should not broadcast the message when forwards is over max', function(){
        let sub1 = {name: 'Subscription1', endpoint: 'http://sub1.com/msg'};
        let sub2 = {name: 'Subscription2', endpoint: 'http://sub2.com/msgs'};

        let subscriptions = [ sub1, sub2 ];

        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        headers[config.parcelForwardHeader] = config.parcelMaxForwards + 5;
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        let sendMessageMock = sandbox.stub(utils, 'sendMessage', function(endpoint, message){
            expect(message.payload).to.be.defined;
            expect(message.payload.data).to.equal(payload.data);
        });

        utils.broadcastMessage(subscriptions, message);

        sinon.assert.notCalled(sendMessageMock);
    });

    it('should set message headers when sending a broadcasted a message', function(){
        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options){
            expect(options.headers['parcel-test']).to.be.defined;
            return new Promise(function(fulfill){
                fulfill({statusCode: 200});
            });
        });

        utils.sendMessage(endpoint, message);
    });

    it('should send the message payload as the JSON body for a broadcasted message', function(){
        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options, cb){
            expect(options.json).to.equal(true);
            expect(options.body.data).to.equal(payload.data);

            cb(null, {statusCode: 200});
        });

        utils.sendMessage(endpoint, message);
    });

    it('should send the message payload as the JSON body for a broadcasted message', function(){

        sandbox.stub(config, 'broadcastTimeout', undefined);

        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options, cb){
            expect(options.json).to.equal(true);
            expect(options.body.data).to.equal(payload.data);

            cb(null, {statusCode: 200});
        });

        utils.sendMessage(endpoint, message);
    });

    it('should handle a failed broadcast due to server error without blowing up', function(){
        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options, cb ){
            expect(options.json).to.equal(true);
            expect(options.body.data).to.equal(payload.data);

            cb(null, {statusCode: 500});
        });

        utils.sendMessage(endpoint, message);
    });

    it('should handle a failed broadcast due to bad request without blowing up', function(){
        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options, cb){
            console.log('hahahaha');
            expect(options.json).to.equal(true);
            expect(options.body.data).to.equal(payload.data);

            cb(null, {statusCode: 400});
        });

        utils.sendMessage(endpoint, message);
    });

    it('should handle a failed broadcast due to a system error without blowing up', function(){
        let payload = {data:'Some data'};
        let headers = {'parcel-test':true};
        let message = new Message({headers: headers, payload:payload});
        let endpoint = 'http://fake.test.com/messages';

        sandbox.stub(request, 'post', function(options, cb){
            console.log('hahahaha');
            expect(options.json).to.equal(true);
            expect(options.body.data).to.equal(payload.data);

            cb({error: 'random stuff happened'}, null);
        });

        utils.sendMessage(endpoint, message);
    });

    it('should default to an inner error when no default is provided to processMongooseError', function(){
        let ex = {
            errors: {
                name: {
                    message: 'must be unique',
                    properties: {
                        type: 'unique'
                    }
                }
            }
        };

        let error = utils.processMongooseError(ex);

        expect(error).to.be.defined;
        expect(error.code).to.be.defined;
        expect(error.code).to.equal(409);
        expect(error.errorBody).to.be.defined;
    });

    it('should default to an inner error when no default is provided to processMongooseError', function(){
        let ex = {
            message:"this should blow up"
        };

        let error = utils.processMongooseError(ex);

        expect(error).to.be.defined;
        expect(error.code).to.be.defined;
        expect(error.code).to.equal(500);
        expect(error.errorBody).to.be.defined;
    });

    it('should use the default error when one is provided to processMongooseError', function(){
        let ex = {
            errors: {
                name: {properties: {type:'testType'}}
            }
        };

        let defaultError = {code:999, errorBody: {
            message: 'An error occurred while pulling queues',
            error: {error: "SomeError", message:"Something bad happened"}
        }};

        let error = utils.processMongooseError(ex, defaultError);

        expect(error).to.be.defined;
        expect(error.code).to.be.defined;
        expect(error.code).to.equal(999);
        expect(error.errorBody).to.be.defined;
    });

    it('should use the inner error when no default is provided to processMongooseError', function(){
        let ex = {
            errors: {
                name: {properties: {type:'testType'}}
            }
        };

        let defaultError = {code:999, errorBody: {
            message: 'An error occurred while pulling queues',
            error: {error: "SomeError", message:"Something bad happened"}
        }};

        let error = utils.processMongooseError(ex);

        expect(error).to.be.defined;
        expect(error.code).to.be.defined;
        expect(error.code).to.equal(500);
        expect(error.errorBody).to.be.defined;
    });
});