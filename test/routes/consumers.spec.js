/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
var consumerRoutes = require('../../routes/consumer-routes');
var Consumer = require('../../models/consumer');
var Queue = require('../../models/queue');
var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');
var config = require('../../config');
var utils = require('../../utils');


describe('Consumer Routes', function() {

    mochaGen.install();

    var sandbox;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
    })

    afterEach(function(){
        sandbox.restore();
    });

    it('should return a saved consumer with no token when registering to an open queue', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url:'http://con.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            json: function(object){
                expect(object.name).to.equal(consumer.name);
                expect(object.subscription.url).to.equal(consumer.subscription.url);
                expect(object._id).to.be.defined;
            },
            status: function(code){
                expect(code).to.not.exist;
            }
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let consumeStub = sandbox.stub(Consumer, 'consume', function* (subQueue, newConsumer){
            expect(subQueue.name).to.equal(queue.name);
            expect(newConsumer.name).to.equal(consumer.name);
            expect(newConsumer.subscription.url).to.equal(consumer.subscription.url);
            newConsumer._id = mongoose.Types.ObjectId();
            return newConsumer;
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.calledOnce(consumeStub);
    });

    it('should return a 401 registering to a secured queue and passing no/invalid token', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.token, token: "faketoken"});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url:'http://con.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            json: function(object){ },
            status: function(code){ },
        };

        let loadMock = sandbox.stub(Queue, 'load', function* (){return queue});

        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
        });
        let statusMock = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(401);
            return this;
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.calledOnce(loadMock);
        sinon.assert.calledOnce(statusMock);
        sinon.assert.calledOnce(jsonMock);
    });

    it('should return a 415 when Content-type is NOT application/json', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url:'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/pdf'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            json: function(object){
                expect(object.name).to.equal(consumer.name);
                expect(object.subscription.url).to.equal(consumer.subscription.url);
                expect(object._id).to.be.defined;
            },
            status: function(code){
                expect(code).to.equal(415);
                return {
                    json: function(content) {}
                }
            }
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let consumeStub = sandbox.stub(Consumer, 'consume', function (subQueue, newConsumer){
            expect(subQueue.name).to.equal(queue.name);
            expect(newConsumer.name).to.equal(consumer.name);
            expect(newConsumer.endpoint).to.equal(consumer.subscription.url);
            newConsumer._id = mongoose.Types.ObjectId();
            return utils.makeObjectPromise(newConsumer);
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.notCalled(consumeStub);
    });


    it('should return a 404 when consuming a queue that cannot be found', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url:'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            json: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return null});

        let consumeStub = sandbox.stub(Consumer, 'consume', function* (subQueue, newConsumer){
            return newConsumer;
        });

        let jsonStub = sandbox.stub(res, 'json', function(object){
            expect(object).to.be.defined;
            expect(object.message).to.be.defined;
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(404);
            return this;
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.notCalled(consumeStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should handle a parcel error and return proper error code when consuming', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url: 'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            json: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let consumeStub = sandbox.stub(Consumer, 'consume', function* (subQueue, newConsumer){
            throw config.errors.queueRequired;
        });

        let jsonStub = sandbox.stub(res, 'json', function(object){
            expect(object).to.be.defined;
            expect(object.message).to.be.defined;
            expect(object.message).to.equal(config.errors.queueRequired.message);
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(config.errors.queueRequired.code);
            return this;
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.calledOnce(consumeStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should handle a non-parcel error and return 500 error code when consuming', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let url = '/api/queues/OpenQueue1/consumers';

        let consumer = {name: 'con1', subscription: {url: 'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name
            },
            body: consumer,
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };

        let res = {
            send: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let randomError = {error: 'SomeError', message: 'Really, though, you did nothing wrong.'};

        let consumeStub = sandbox.stub(Consumer, 'consume', function* (subQueue, newConsumer){
            throw randomError;
        });

        let jsonStub = sandbox.stub(res, 'send', function(object){
            expect(object).to.be.defined;
            expect(object.message).to.be.defined;
            expect(object.error.message).to.equal(randomError.message);
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(500);
            return this;
        });

        yield consumerRoutes.postConsumerOfQueue(req, res);

        sinon.assert.calledOnce(consumeStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should remove a consumer from a valid parcel queue and return the removed consumer', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let consumer = {name: 'con1', subscription: {url: 'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name,
                conname: consumer.name
            },
            headers: {}
        };

        let res = {
            json: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let consumeStub = sandbox.stub(Consumer, 'regurgitate', function* (subQueue, consumerName){
            expect(subQueue.name).to.equal(queue.name);
            expect(consumerName).to.equal(consumer.name);
            return consumer;
        });

        let jsonStub = sandbox.stub(res, 'json', function(object){
            expect(object).to.be.defined;
            expect(object.name).to.equal(consumer.name);
            return this;
        });

        yield consumerRoutes.deleteConsumerOfQueue(req, res);

        sinon.assert.calledOnce(consumeStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should return a 404 when unsubscribing from a queue that does not exist', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let consumer = {name: 'con1', subscription: {url: 'http://con1.com/msgs'}};

        let req = {
            params: {
                name: queue.name,
                conname: consumer.name
            },
            headers: {}
        };

        let res = {
            json: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return null});

        let regurgStub = sandbox.stub(Consumer, 'regurgitate', function* (subQueue, consumerName){
            return consumer;
        });

        let jsonStub = sandbox.stub(res, 'json', function(object){
            expect(object).to.be.defined;
            expect(object.message).to.be.defined;
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(404);
            return this;
        });

        yield consumerRoutes.deleteConsumerOfQueue(req, res);

        sinon.assert.notCalled(regurgStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should return a 204 when removing a consumer that does not exist', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let consumer = {name: 'sub1', subscription:{url: 'http://sub1.com/msgs'}};

        let req = {
            params: {
                name: queue.name,
                conname: consumer.name
            },
            headers: {}
        };

        let res = {
            send: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let regurgStub = sandbox.stub(Consumer, 'regurgitate', function* (subQueue, consumerName){
            return null;
        });

        let jsonStub = sandbox.stub(res, 'send', function(object){
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(204);
            return this;
        });

        yield consumerRoutes.deleteConsumerOfQueue(req, res);

        sinon.assert.calledOnce(regurgStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should handle a parcel error and return the proper error code', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let consumer = {name: 'sub1', subscription: {url:'http://sub1.com/msgs'}};

        let req = {
            params: {
                name: queue.name,
                conname: consumer.name
            },
            headers: {}
        };

        let res = {
            json: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let regurgStub = sandbox.stub(Consumer, 'regurgitate', function* (subQueue, consumerName){
            throw config.errors.queueRequired;
        });

        let jsonStub = sandbox.stub(res, 'json', function(object){

            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(config.errors.queueRequired.code);
            return this;
        });

        yield consumerRoutes.deleteConsumerOfQueue(req, res);

        sinon.assert.calledOnce(regurgStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should handle a parcel error and return the proper error code', function* (){
        let queue = new Queue({name:'OpenQueue1', security: config.queueSecurity.none});

        let consumer = {name: 'sub1', subscription: {url:'http://sub1.com/msgs'}};

        let req = {
            params: {
                name: queue.name,
                conname: consumer.name
            },
            headers: {}
        };

        let res = {
            send: function(object){},
            status: function(code){}
        };

        sandbox.stub(Queue, 'load', function* (){return queue});

        let randomError = {error: 'SomeError', message: 'Really, though, you did nothing wrong.'};

        let regurgStub = sandbox.stub(Consumer, 'regurgitate', function* (subQueue, consumerName){
            throw randomError;
        });

        let jsonStub = sandbox.stub(res, 'send', function(object){
            expect(object.error.message).to.equal(randomError.message);
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(500);
            return this;
        });

        yield consumerRoutes.deleteConsumerOfQueue(req, res);

        sinon.assert.calledOnce(regurgStub);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(jsonStub);
    });

    it('should return 404 when loading consumers for a non-existant queue', function* (){
        let id = mongoose.Schema.Types.ObjectId();
        let queue = new Queue({name:'testQueue', security: config.queueSecurity.none});

        let req = {
            params: {
                name: queue.name
            },
            header:function(){}
        }

        let res = {
            json: function(){},
            status: function(){},
            send: function(){}
        }

        sandbox.stub(req, 'header', function(){return null});
        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
        });
        let loadMock = sandbox.stub(Queue, 'load', function(queueName){
            return utils.makeObjectPromise(null);
        });
        let findMock = sandbox.stub(Consumer, 'find', function(query){
            return utils.makeObjectPromise(null);
        });
        let statusMock = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(404);
            return this;
        });
        let sendMock = sandbox.stub(res, 'send', function(obj){

        });

        yield consumerRoutes.getConsumersForQueue(req, res);

        sinon.assert.notCalled(findMock);
        sinon.assert.calledOnce(loadMock);
        sinon.assert.calledOnce(statusMock);
    });

    it('should handle a mongoose error when loading consumers', function* (){
        let id = mongoose.Schema.Types.ObjectId();
        let queue = new Queue({name:'testQueue', security: config.queueSecurity.none});

        let req = {
            params: {
                name: queue.name
            },
            header:function(){}
        }

        let res = {
            json: function(){},
            status: function(){},
            send: function(){}
        }

        sandbox.stub(req, 'header', function(){return null});
        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
        });
        let loadMock = sandbox.stub(Queue, 'load', function(queueName){
            throw {
                errors: {
                    name: {
                        message: 'something bad happened',
                        properties: {
                            type: 'something'
                        }
                    }
                }
            };
        });
        let findMock = sandbox.stub(Consumer, 'find', function(query){
            return utils.makeObjectPromise(null);
        });
        let statusMock = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(500);
            return this;
        });
        let sendMock = sandbox.stub(res, 'send', function(obj){

        });

        yield consumerRoutes.getConsumersForQueue(req, res);

        sinon.assert.notCalled(findMock);
        sinon.assert.calledOnce(loadMock);
        sinon.assert.calledOnce(statusMock);
    });

    it('should return 401 when loading consumers for a queue with security and no token is provided', function* (){
        let id = mongoose.Schema.Types.ObjectId();
        let queue = new Queue({name:'testQueue', security: config.queueSecurity.token});

        let req = {
            params: {
                name: queue.name
            },
            header:function(){}
        }

        let res = {
            json: function(){},
            status: function(){},
            send: function(){}
        }

        sandbox.stub(req, 'header', function(){return null});
        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
        });
        let loadMock = sandbox.stub(Queue, 'load', function(queueName){
            return utils.makeObjectPromise(queue);
        });
        let findMock = sandbox.stub(Consumer, 'find', function(query){
            return utils.makeObjectPromise(null);
        });
        let statusMock = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(401);
            return this;
        });
        let sendMock = sandbox.stub(res, 'send', function(obj){

        });

        yield consumerRoutes.getConsumersForQueue(req, res);

        sinon.assert.notCalled(findMock);
        sinon.assert.calledOnce(loadMock);
        sinon.assert.calledOnce(statusMock);
    });

    it('should return 204 when loading consumers for a queue and no consumers are found', function* (){
        let id = mongoose.Schema.Types.ObjectId();
        let queue = new Queue({name:'testQueue', security: config.queueSecurity.none});

        let req = {
            params: {
                name: queue.name
            },
            header:function(){}
        }

        let res = {
            json: function(){},
            status: function(){},
            send: function(){}
        }

        sandbox.stub(req, 'header', function(){return null});
        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
        });
        let loadMock = sandbox.stub(Queue, 'load', function(queueName){
            return utils.makeObjectPromise(queue);
        });
        let findMock = sandbox.stub(Consumer, 'find', function(query){
            return utils.makeObjectPromise(null);
        });
        let statusMock = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(204);
            return this;
        });
        let sendMock = sandbox.stub(res, 'send', function(obj){

        });

        yield consumerRoutes.getConsumersForQueue(req, res);

        sinon.assert.calledOnce(findMock);
        sinon.assert.calledOnce(loadMock);
        sinon.assert.calledOnce(statusMock);
    });

    it('should return consumers for a given queue', function* (){
        let id = mongoose.Schema.Types.ObjectId();
        let queue = new Queue({name:'testQueue', security: config.queueSecurity.none});
        let consumers = [{name:'con1',subscription: {url:'http://localhost:1234/test'},_id:id}];

        let req = {
            params: {
                name: queue.name
            },
            header:function(){}
        }

        let res = {
            json: function(){}
        }

        sandbox.stub(req, 'header', function(){return null});
        let jsonMock = sandbox.stub(res, 'json', function(obj){
            expect(obj[0]).to.be.defined;
            expect(obj[0].name).equal("con1");
        });
        let loadMock = sandbox.stub(Queue, 'load', function(queueName){
            expect(queueName).to.equal(queue.name);
            return utils.makeObjectPromise(queue);
        });
        let findMock = sandbox.stub(Consumer, 'find', function(query){
            return utils.makeObjectPromise(consumers);
        });

        yield consumerRoutes.getConsumersForQueue(req, res);

        sinon.assert.calledOnce(jsonMock);
        sinon.assert.calledOnce(findMock);
    });

});