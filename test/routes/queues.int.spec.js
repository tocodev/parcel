/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var queueRoutes = require('../../routes/queue-routes');
var queues = require('../../routes/queues');
const mongoose = require('mongoose');
var Queue = require('../../models/queue');
var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');
var request = require('supertest');
var config = require('../../config');

describe('Queue Routes Integration Test', function () {

    mochaGen.install();
    var app;

    var openQueue1 = {
        name: 'OpenQueue1'
    };

    var secQueue1 = {
        name: 'SecQueue1',
        security: config.queueSecurity.token
    };
    var secQueue1Token = null;

    let openSub1 = {name: 'IntSub1', endpoint: 'http://intsub.parcelrq.com'};

    let openCon1 = {name: 'IntCon1', subscription:{url: 'http://intcon.parcelrq.com'}};

    let openMsg1 = {
        headers: {},
        payload: {name: 'Arthur Baker', address: '104 Scotts Dr. Abington PA 16334', phone: '8813041922'}
    };

    before(function*() {
        let mongoHost = process.env.MONGO_HOST || 'localhost:27017';
        console.log('Connecting to mongo host: '+mongoHost);
        yield mongoose.connect(mongoHost + '/parcelTest');
        app = require('../../app');
    })

    beforeEach(function () {

    });

    after(function*() {
        mongoose.connection.db.dropDatabase();
        yield mongoose.connection.close();
    });

    it('Should return a 201 CREATED and saved queue with default security when a new queue is created from only a queue name', function (done) {

        request(app)
            .post('/api/queues')
            .send(openQueue1)
            .expect(201)
            .expect('Content-Type', /json/)
            .then(response => {
                expect(response.body._id).to.be.defined;
                expect(response.body.name).to.equal(openQueue1.name);
                expect(response.body.security).to.equal(config.queueSecurity.none);
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a 400 BAD REQUEST when a POST is made for a new queue without required information', function (done) {

        request(app)
            .post('/api/queues')
            .send({junk:'data'})
            .expect(400)
            .expect('Content-Type', /json/)
            .then(response => {
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a 415 when doing a POST to a queue without content type json', function (done) {

        request(app)
            .post('/api/queues')
            .send('TESTDATA')
            .set('Content-Type', 'application/text')
            .expect(415)
            .then(response => {
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a 500 when a POST is made for a new queue when unhandled error occurs', function (done) {

        request(app)
            .post('/api/queues')
            .send({name:{a:4}})
            .expect(500)
            .expect('Content-Type', /json/)
            .then(response => {
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a 201 CREATED and saved queue with a token when a new queue is created with security', function (done) {

        request(app)
            .post('/api/queues')
            .send(secQueue1)
            .expect(201)
            .then(response => {
                let savedQueue = response.body;
                expect(savedQueue).to.be.defined;
                expect(savedQueue.name).to.equal(secQueue1.name);
                expect(savedQueue.security).to.equal(config.queueSecurity.token);
                expect(savedQueue.token).to.be.defined;
                secQueue1Token = savedQueue.token;

                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a 409 CONFLICT and an error message when attempting a queue that already exists', function (done) {

        request(app)
            .post('/api/queues')
            .send(openQueue1)
            .expect(409)
            .then(response => {
                expect(response.body.message).to.be.defined;
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a list of queues when doing a GET with no queue name', function (done) {

        request(app)
            .get('/api/queues')
            .expect(200)
            .then(response => {
                let queues = response.body;
                expect(queues).to.be.defined;
                expect(queues[0].name).to.equal(openQueue1.name);
                expect(queues[0].security).to.equal(config.queueSecurity.none);
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return an open queue when requesting a queue spec by name', function (done) {

        request(app)
            .get('/api/queues/' + openQueue1.name)
            .expect(200)
            .then(response => {
                let queue = response.body;
                expect(queue).to.be.defined;
                expect(queue.name).to.equal(openQueue1.name);
                expect(queue.security).to.equal(config.queueSecurity.none);
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a secure queue when requesting a queue spec by name that had security', function (done) {
        request(app)
            .get('/api/queues/' + secQueue1.name)
            .set('token', secQueue1Token)
            .expect(200)
            .then(response => {
                let queue = response.body;
                expect(queue).to.be.defined;
                expect(queue.name).to.equal(secQueue1.name);
                expect(queue.security).to.equal(config.queueSecurity.token);
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return an open queue when deleting an open queue by name', function (done) {

        done();
    });

    it('Should return a secure queue when deleting a queue by name that had security', function (done) {

        done();
    });

    it('should return a saved message when posting a message to an existing open queue', function (done) {
        request(app)
            .post('/api/queues/'+openQueue1.name+'/messages')
            .send(openMsg1.payload)
            .expect(200)
            .then(response => {
                let messagePayload = response.body;
                expect(messagePayload).to.be.defined;
                expect(messagePayload.name).to.equal(openMsg1.payload.name);
                expect(messagePayload.address).to.equal(openMsg1.payload.address);
                expect(messagePayload.phone).to.equal(openMsg1.payload.phone);
                done();
            }, err => {
                done(err);
            });
    });

    it('should return a saved message when posting a message to an new queue', function (done) {
        request(app)
            .post('/api/queues/freshQueue/messages')
            .send(openMsg1.payload)
            .expect(200)
            .then(response => {
                let messagePayload = response.body;
                expect(messagePayload).to.be.defined;
                expect(messagePayload.name).to.equal(openMsg1.payload.name);
                expect(messagePayload.address).to.equal(openMsg1.payload.address);
                expect(messagePayload.phone).to.equal(openMsg1.payload.phone);
                done();
            }, err => {
                done(err);
            });
    });

    it('should return a 400 BAD REQUEST when posting a message without payload to a queue', function (done) {
        request(app)
            .post('/api/queues/freshQueue/messages')
//            .send(undefined)
            .set('Content-Type','application/json')
            .expect(400)
            .then(response => {
                done();
            }, err => {
                done(err);
            });
    });

    it('should return a saved message when reading a message from an existing open queue', function (done) {
        request(app)
            .get('/api/queues/'+openQueue1.name+'/messages')
            .expect(200)
            .then(response => {
                let messagePayload = response.body;
                expect(messagePayload).to.be.defined;
                expect(messagePayload.name).to.equal(openMsg1.payload.name);
                expect(messagePayload.address).to.equal(openMsg1.payload.address);
                expect(messagePayload.phone).to.equal(openMsg1.payload.phone);
                done();
            }, err => {
                done(err);
            });
    });

    it('should return a valid consumer when registering to consume an existing open queue', function (done) {
        request(app)
            .post('/api/queues/'+openQueue1.name+'/consumers')
            .send(openCon1)
            .expect(200)
            .then(response => {
                let savedCon = response.body;
                expect(savedCon).to.be.defined;
                expect(savedCon.name).to.equal(openCon1.name);
                expect(savedCon.endpoint).to.equal(openCon1.endpoint);
                expect(savedCon._id).to.be.defined;
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return all consumers when loading for an existing open queue', function(done){
        request(app)
            .get('/api/queues/'+openQueue1.name+'/consumers')
            .expect(200)
            .then(response => {
                let cons = response.body;
                expect(cons.length).to.equal(1);
                expect(cons[0].name).to.equal(openCon1.name);
                done();
            }, err => {
                done(err);
            });
    })

    it('should return a valid consumer when removing an existing consumer', function (done) {
        request(app)
            .delete('/api/queues/'+openQueue1.name+'/consumers/'+openCon1.name)
            .expect(200)
            .then(response => {
                let savedCon = response.body;
                expect(savedCon).to.be.defined;
                expect(savedCon.name).to.equal(openCon1.name);
                expect(savedCon.endpoint).to.equal(openCon1.endpoint);
                expect(savedCon._id).to.be.defined;
                done();
            }, err => {
                done(err);
            });
    });

    it('', function (done) {
        // Fill in test functionality here
        done();
    });

    it('Should blow up when trying to delete a queue by name that had security while passing no token', function (done) {

        done();
    });

    it('Should return 204 NO CONTENT when trying to delete a named queue that does not exist', function (done) {

        request(app)
            .delete('/api/queues/AQUEUENAMETHATDOESNOTEXIST')
            .expect(204)
            .then(response => {
                done();
            }, err => {
                done(err);
            });
    });

    it('Should return a deleted queue when doing a DELETE against an existing queue', function (done) {

        request(app)
            .delete('/api/queues/' + openQueue1.name)
            .expect(200)
            .then(response => {
                let queue = response.body;
                expect(queue).to.be.defined;
                expect(queue.name).to.equal(openQueue1.name);
                expect(queue.security).to.equal(config.queueSecurity.none);
                done();
            }, err => {
                done(err);
            });
    });

});