/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var queueRoutes = require('../../routes/queue-routes');
var queues = require('../../routes/queues');
var Queue = require('../../models/queue');
var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');
var config = require('../../config');
var utils = require('../../utils');


describe('Queue Routes', function(){

    mochaGen.install();

    var sandbox;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
    })

    afterEach(function(){
        sandbox.restore();
    });

    it('should return only the necessary info for a GET on queues', function* (){
        sandbox.stub(Queue, 'find', function(){
            return [
                {name:'TestQueue1', security: config.queueSecurity.none, messages: [{stuff:'some stuff'}]}
            ];
        });

        let req = {};
        let res = {json: function(object){
            expect(object.constructor === Array).to.equal(true);

            let queue = object[0];
            expect(queue).to.be.defined;
            expect(queue.name).to.equal('TestQueue1');
            expect(queue.messages).to.be.undefined;
        }};

        let send = res.send = sinon.spy();

        yield queueRoutes.getAllQueues(req, res);
    });

    function UserException(message) {
        this.message = message;
        this.name = 'UserException';
    }

    it('should return 500 when exception occurs during a GET on queues', function* (){
        sandbox.stub(Queue, 'find', function(){
            throw new UserException("lame");
        });

        let req = {};
        let res = {status: function(status){
            expect(status).to.equal(500);
            return {send: function(){}};
        }};

        let send = res.send = sinon.spy();

        yield queueRoutes.getAllQueues(req, res);
    });

    it('should return a 204 NO CONTENT for a GET on queues when no queues exist', function* (){
        sandbox.stub(Queue, 'find', function* (){
            return null;
        });

        let req = {};
        let res = {
            send: function(object){},
            status: function(code){
                expect(code).to.equal(204);
                return this;
            }
        };



        let sendStub = sandbox.stub(res, 'send', function(object){
            expect(object).to.not.be.defined;
            return this;
        });

        yield queueRoutes.getAllQueues(req, res);

        sinon.assert.calledOnce(sendStub);
    });

    it('should handle a GET on queues when there are no queues', function* (){
        sandbox.stub(Queue, 'find', function(){ return []});

        let req = {};
        let res = {
            json: function(object){
                expect(object.constructor === Array).to.equal(true);
                expect(object.length).to.equal(0);
            },
            status: function(code){
                expect(code).to.equal(204);
            }
        };

        yield queueRoutes.getAllQueues(req, res);
    });

    it('should return a single queue for a GET on queues with a name url parameter', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
            messages: [],

            clean: function() {return {}}
        }

        sandbox.stub(Queue, 'load', function* (name){return queue});

        let req = {
            params: {
                name: 'TestQueue'
            },
            headers: {}
        };
        let res = {
            json: function(object){
                expect(object.name).to.equal(queue.name);
            }
        };

        yield queueRoutes.getQueueByName(req,res);
    });

    it('should return a 404 for a GET on queues when queue not found', function* (){

        sandbox.stub(Queue, 'load', function* (name){return undefined});

        let req = {
            params: {
                name: 'TestQueue'
            },
            headers: {}
        };
        let res = {status: function(status){
            expect(status).to.equal(404);
            return {send: function(){}};
        }};

        yield queueRoutes.getQueueByName(req,res);
    });

    it('should return a 401 for a GET on queues with an invalid token', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.token,
            messages: [],
            token: '12345',

            clean: function() {return {}}
        }

        sandbox.stub(Queue, 'load', function* (name){return queue});

        let req = {
            params: {
                name: 'TestQueue'
            },
            headers: {token:'ABCDE'},
            header: function(key) {
                return headers[key];
            }
        };
        let res = {status: function(status){
            console.log("ST:"+status);
            expect(status).to.equal(401);
            return {json: function(){}};
        }};

        yield queueRoutes.getQueueByName(req,res);
    });

    it('should return 500 when exception occurs during a GET on queues by queue name', function* (){
        sandbox.stub(Queue, 'load', function(){
            throw new UserException("lame");
        });

        let req = {
            params: {
                name: 'TestQueue'
            },
            headers: {
                token: 'ABC123'
            }
        };
        let res = {status: function(status){
            expect(status).to.equal(500);
            return {send: function(){}};
        }};

        let send = res.send = sinon.spy();

        yield queueRoutes.getQueueByName(req,res);
    });

    it('should return the deleted queue and a 200 status when deleting by name', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let deleteStub = sandbox.stub(Queue, 'delete', function* (name){
            expect(name).to.equal(queue.name);
            return queue;
        });

        let req = {
            params: {
                name: queue.name
            },
            headers: {}
        };
        let res = {
            json: function(object){
                expect(object.name).to.equal(queue.name);
                expect(object.security).to.equal(config.queueSecurity.none);
                return this;
            },
            status: function(code){
                expect(code).to.equal(200);
                return this;
            }
        };

        yield queueRoutes.deleteQueueByName(req, res);

        sinon.assert.calledOnce(deleteStub);
    });

    it('should return a 204 status when deleting by name a queue that is not found', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let deleteStub = sandbox.stub(Queue, 'delete', function* (name){
            expect(name).to.equal(queue.name);
            return null;
        });

        let req = {
            params: {
                name: queue.name
            },
            headers: {}
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){
                expect(code).to.equal(204);
                return this;
            }
        };

        let jsonStub = sinon.stub(res, 'json', function(object){
            return this;
        });

        let sendStub = sinon.stub(res, 'send', function(object){
            return this;
        });

        yield queueRoutes.deleteQueueByName(req, res);

        sinon.assert.calledOnce(deleteStub);
        sinon.assert.calledOnce(sendStub);
        sinon.assert.notCalled(jsonStub);
    });

    it('should handle errors and return a 500 status when something fails unexpectedly', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let someError = {error: 'SomeError', message: 'This is just a test message'};

        let deleteStub = sandbox.stub(Queue, 'delete', function* (name){
            expect(name).to.equal(queue.name);
            throw someError;
        });

        let req = {
            params: {
                name: queue.name
            },
            headers: {}
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){
                expect(code).to.equal(500);
                return this;
            }
        };

        let jsonStub = sinon.stub(res, 'json', function(object){
            return this;
        });

        let sendStub = sinon.stub(res, 'send', function(object){
            expect(object.message).to.be.defined;
            expect(object.error.error).to.equal(someError.error);
            expect(object.error.message).to.equal(someError.message);
            return this;
        });

        yield queueRoutes.deleteQueueByName(req, res);

        sinon.assert.calledOnce(deleteStub);
        sinon.assert.calledOnce(sendStub);
        sinon.assert.notCalled(jsonStub);
    });

    it('Should return a 500 when handling a strange mongoose error on POSTing a queue', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let req = {
            body: queue,
            headers: {},
            header: function(header){
                return 'application/json';
            }
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){
                expect(code).to.equal(500);
                return this;
            }
        };

        sandbox.stub(Queue.prototype, 'save', function(){
            throw {errors: {}, message:'oh no'};
        });

        yield queueRoutes.postQueue(req, res);
    });

    it('Should return a 415 when POSTing a queue with incorrect content type', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let req = {
            body: queue,
            headers: {},
            header: function(header){
                return null;
            }
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){}
        };

        sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(415);
            return this;
        });

        sandbox.stub(res, 'json', function(obj){
            expect(obj.message).to.be.defined;
            return this;
        });

        sandbox.stub(Queue.prototype, 'save', function(){
            throw {errors: {}, message:'oh no'};
        });

        yield queueRoutes.postQueue(req, res);
    });

    it('Should return a 201 when POSTing a queue with valid content type', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.none,
        };

        let req = {
            body: queue,
            headers: { 'Content-Type':'application/json' },
            header: function(header){
                return this.headers[header];
            }
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){}
        };

        sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(201);
            return this;
        });

        sandbox.stub(res, 'json', function(obj){
            expect(obj.name).to.equal(queue.name);
            return this;
        });

        sandbox.stub(Queue.prototype, 'save', function(){
            return utils.makeObjectPromise({});
        });

        yield queueRoutes.postQueue(req, res);
    });

    it('Should return a 201 and queue with token when POSTing a queue with valid content type and security token', function* (){
        let queue = {
            name: 'TestQueue',
            security: config.queueSecurity.token,
        };

        let tokenValue = 'THISISAFAKETOKEN';

        let req = {
            body: queue,
            headers: { 'Content-Type':'application/json' },
            header: function(header){
                return this.headers[header];
            }
        };
        let res = {
            json: function(object){},
            send: function(){},
            status: function(code){}
        };

        sandbox.stub(Queue.prototype, 'generateToken', function(){
            this.token = tokenValue;
        });

        sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(201);
            return this;
        });

        sandbox.stub(res, 'json', function(obj){
            expect(obj.name).to.equal(queue.name);
            expect(obj.token).to.equal(tokenValue);
            return this;
        });

        sandbox.stub(Queue.prototype, 'save', function(){
            return utils.makeObjectPromise({});
        });

        yield queueRoutes.postQueue(req, res);
    });
});