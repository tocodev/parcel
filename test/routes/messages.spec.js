/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var messageRoutes = require('../../routes/message-routes');
var messages = require('../../routes/messages');
var Queue = require('../../models/queue');
var Message = require('../../models/message')
// var Subscription = require('../../models/subscription')
var Consumer = require('../../models/consumer');
var utils = require('../../utils');

var chai = require('chai');
var expect = chai.expect;
var mochaGen = require('mocha-generators');
var sinon = require('sinon');
var config = require('../../config');

describe('Message Routes', function() {

    mochaGen.install();

    var sandbox;

    beforeEach(function(){
        sandbox = sinon.sandbox.create();
        sandbox.stub(Message, 'clean', function(){
            return utils.makeObjectPromise({});
        });
    });

    afterEach(function(){
        sandbox.restore();
    });

    it('should get a single message and its headers and queue depth for a GET call', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerGet"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });

        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){
                expect(object.message).to.be.defined;
                expect(object.message).to.equal(testPayload.message);
            },
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        expect(res.headers['Queue-Depth']).to.equal(3);
        expect(res.headers['header']).to.equal("headerGet");
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should get a single message and its headers and queue depth for a GET call', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerGet"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });

        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){
                expect(object.message).to.be.defined;
                expect(object.message).to.equal(testPayload.message);
            },
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        expect(res.headers['Queue-Depth']).to.equal(3);
        expect(res.headers['header']).to.equal("headerGet");
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should get a single message and its headers and queue depth for a GET call on a queue with the parcelMessage flag', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none, parcelMessage: true};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerGet"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });

        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){
                expect(object.message).to.be.defined;
                expect(object.message).to.equal(testPayload.message);
                expect(object.headers).to.be.defined;
                expect(object.createdDate).to.be.defined;
                expect(object.headers.header).to.equal('headerGet');
            },
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        expect(res.headers['Queue-Depth']).to.equal(3);
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should return a 404 for a GET call on a non-existent queue', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return null});

        let req = {
            params: {name: 'TestQueue'},
            headers: {}
        };
        let res = {
            headers: {},
            json: function(object){ },
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(code) { }
        };

        let jsonStub = sandbox.stub(res, 'json', function(object){
            expect(object.message).to.be.defined;
            return this;
        });

        let statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(404);
            return this;
        });

        yield messageRoutes.getMessageFromQueue(req, res);
        sinon.assert.calledOnce(jsonStub);
        sinon.assert.calledOnce(statusStub);
    });

    it('should return a 204 for a GET call on a queue that has no messages left', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function*(queueId, method) {
            return null;
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 0;
        });

        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(code) {},
            send: function(object) {}
        };

        var statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(204);
            return this;
        });

        var sendStub = sandbox.stub(res, 'send', function(object){
            return this;
        })

        yield messageRoutes.getMessageFromQueue(req, res);
        expect(res.headers['Queue-Depth']).to.equal(0);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should handle an error when calling GET on a queue', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        let error = {error: 'SomeError', message: 'Something bad happened'};

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function*(queueId, method) {
            throw error;
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload,
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 0;
        });

        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(code) {},
            send: function(object) {}
        };

        var statusStub = sandbox.stub(res, 'status', function(code){
            expect(code).to.equal(500);
            return this;
        });

        var sendStub = sandbox.stub(res, 'send', function(object){
            expect(object).to.be.defined;
            expect(object.message).to.be.defined;
            expect(object.error.message).to.equal(error.message);
            return this;
        })

        yield messageRoutes.getMessageFromQueue(req, res);
        sinon.assert.calledOnce(statusStub);
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should browse a single message and its headers and queue depth for a GET call with browse header', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerGet"}, payload:testPayload, 
                createdDate:new Date(), expiry:5000};
        });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {
            return {queueId:queueId, headers:{header:"headerBrowse"}, payload:testPayload, 
                createdDate:new Date(), expiry:5000};
        });
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });

        let req = {
            params: {name: 'TestQueue'},
            query: {browse: 'true'},
            //headers: {'parcel-queue-browse': 'true'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            json: function(object){
                expect(object.message).to.be.defined;
                expect(object.message).to.equal(testPayload.message);
            },
            setHeader: function(header, value){
                this.headers[header] = value
            },
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        expect(res.headers['Queue-Depth']).to.equal(3);
        expect(res.headers['header']).to.equal("headerBrowse");
        sinon.assert.calledOnce(messageBrowseMock);
        sinon.assert.notCalled(messageGetMock);
    });

    it('should return a 400 error when passing a bad method for GET message', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        let req = {
            params: {name: 'TestQueue'},
            query: {method: 'however-you-want-is-fine'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            status: function(statusCode) {
                expect(statusCode).to.equal(400);
                return {json: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
    });

    it('should return a 400 error when passing a bad browse value for GET message', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var testPayload = {'message':'Test message'};
        let req = {
            params: {name: 'TestQueue'},
            query: {browse: 'yes-please'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            status: function(statusCode) {
                expect(statusCode).to.equal(400);
                return {json: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
    });

    it('should get first message for a queue when called with FIFO header', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            expect(method).to.equal("FIFO");
            return {queueId:queueId, headers:{}} });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {});
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {});

        let req = {
            params: {name: 'TestQueue'},
            query: {method:'FIFO'},
            //headers: {'parcel-queue-method': 'FIFO'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            json: function(object){},
            setHeader: function(header, value){},
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should get first message for a queue when called with LIFO header', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        // Message stubs
        var messageGetMock = sandbox.stub(Message, 'get', function(queueId, method) {
            expect(method).to.equal("LIFO");
            return {queueId:queueId, headers:{}} });
        var messageBrowseMock = sandbox.stub(Message, 'browse', function(queueId, method) {});
        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {});

        let req = {
            params: {name: 'TestQueue'},
            query: {method:'LIFO'},
            //headers: {'parcel-queue-method': 'LIFO'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            json: function(object){},
            setHeader: function(header, value){},
            status: function(statusCode) {
                return {send: function(){}};
            }
        };

        yield messageRoutes.getMessageFromQueue(req, res);
        sinon.assert.calledOnce(messageGetMock);
        sinon.assert.notCalled(messageBrowseMock);
    });

    it('should set 415 status when header does not have application/json for Content-type', function* () {
        let req = {
            params: {name: 'TestQueue'},
            headers: {},
            header: function(key){return this.headers[key];}
        };
        let res = {
            setHeader: function(header, value){},
            status: function(statusCode) {
                expect(statusCode).to.equal(415);
                return {
                    json: function(message){
                        expect(message.message).to.be.defined;
                    }
                };
            }
        };

        yield messageRoutes.postMessageToQueue(req, res);
    });

    it('should save message when message is POSTed to a queue', function* () {
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function*(queue) {
            return [{consumer:'1'},{consumer:'2'}];
        });
        var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
            return putMessage;
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage', 
            function(consumers, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
        });

        let req = {
            params: {name: 'TestQueue'},
            body: {message: 'TestContent'},
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            json: function(message){
                expect(message.message).to.be.defined;
                return {send: function() {}};
            }
        };

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(messageCountMock);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
    });

    it('should save message with headers included when message is POSTed to a queue with parcelMessage enabled', function* () {
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none, parcelMessage: true};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function*(queue) {
            return [{consumer:'1'},{consumer:'2'}];
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
            function(subscriptions, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
            });

        let headerName = 'parcel-ignore';
        let headerContent = 'this should be ignored';

        let savedHeader = 'parcel-saved';
        let savedHeaderContent = 'saved content here';

        let req = {
            params: {name: 'TestQueue'},
            body: {payload: 'TestContent', headers: {}},
            headers: {'Content-Type':'application/json', headerName : headerContent},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            json: function(message){
                expect(message.message).to.be.defined;
                return {send: function() {}};
            }
        };

        var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
            expect(putMessage.headers).to.be.defined;
            expect(putMessage.headers[headerName]).to.not.exist;
            expect(putMessage.headers[savedHeader]).to.be.defined;
            expect(putMessage.headers[savedHeader]).to.equal(savedHeaderContent);
            return putMessage;
        });

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(messageCountMock);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
    });

    it('should save message with headers included and expiry when message is POSTed to a queue with parcelMessage enabled', function* () {
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none, parcelMessage: true};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function*(queue) {
            return [{consumer:'1'},{consumer:'2'}];
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
            function(consumers, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
            });

        let headerName = 'parcel-ignore';
        let headerContent = 'this should be ignored';

        let savedHeader = 'parcel-saved';
        let savedHeaderContent = 'saved content here';

        let expiryHeader = config.messageExpiryHeader;

        let req = {
            params: {name: 'TestQueue'},
            body: {payload: 'TestContent', headers: {}},
            headers: {'Content-Type':'application/json', headerName : headerContent},
            header: function(key){return this.headers[key];}
        };

        req.body.headers[expiryHeader] = '30';
        req.body.headers[savedHeader] = savedHeaderContent;

        let res = {
            headers: {},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            json: function(message){
                expect(message.message).to.be.defined;
                return {send: function() {}};
            }
        };

        var messagePutMock = sandbox.stub(Message, 'put', function(putMessage) {
            expect(putMessage.headers).to.be.defined;
            expect(putMessage.headers[headerName]).to.not.exist;
            expect(putMessage.headers[savedHeader]).to.be.defined;
            expect(putMessage.headers[savedHeader]).to.equal(savedHeaderContent);
            expect(putMessage.headers[expiryHeader]).to.equal('30');
            expect(putMessage.expiry).to.equal(30);
            return utils.makeObjectPromise(putMessage);
        });

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(messageCountMock);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
    });

    it('should save message and not blow up when a message with null headers is POSTed to a queue with parcelMessage enabled', function* () {
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none, parcelMessage: true};
        sandbox.stub(Queue, 'load', function* (name){return queue});

        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function*(queue) {
            return [{consumer:'1'},{consumer:'2'}];
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
            function(consumers, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
            });

        let headerName = 'parcel-ignore';
        let headerContent = 'this should be ignored';

        let savedHeader = 'parcel-saved';
        let savedHeaderContent = 'saved content here';

        let expiryHeader = config.messageExpiryHeader;

        let req = {
            params: {name: 'TestQueue'},
            body: {payload: 'TestContent', headers: null},
            headers: {'Content-Type':'application/json', headerName : headerContent},
            header: function(key){return this.headers[key];}
        };

        let res = {
            headers: {},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            json: function(message){
                expect(message.message).to.be.defined;
                return {send: function() {}};
            }
        };

        var messagePutMock = sandbox.stub(Message, 'put', function(putMessage) {
            expect(putMessage.headers).to.be.defined;
            expect(putMessage.headers[headerName]).to.not.exist;
            expect(Object.keys(putMessage.headers).length).to.equal(0);
            return utils.makeObjectPromise(putMessage);
        });

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(messageCountMock);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
    });

    it('should create a new queue and save the message when POSTing message to a queue that does not exist', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        sandbox.stub(Queue, 'load', function* (name){return null});
        sandbox.stub(Queue.prototype, 'save', function*(){return {}});

        var messageCountMock = sandbox.stub(Message, 'depth', function*(queueId) {
            return 3;
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function*(queue) {
            return [{consumer:'1'},{consumer:'2'}];
        });
        var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
            return putMessage;
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
            function(consumers, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
            });

        let req = {
            params: {name: 'TestQueue'},
            body: {message: 'TestContent'},
            headers: {'Content-Type':'application/json'},
            header: function(key){return this.headers[key];}
        };
        let res = {
            headers: {},
            setHeader: function(header, value){
                this.headers[header] = value
            },
            json: function(message){
                expect(message.message).to.be.defined;
                return {send: function() {}};
            }
        };

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(messageCountMock);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
    });

    it('should return 400 when POSTing a null message to an existing queue', function* (){
            // stub load function
            let queue = {name: 'TestQueue', security: config.queueSecurity.none};
            let loadMock = sandbox.stub(Queue, 'load', function(name) {
                return utils.makeObjectPromise(queue);
            });
            let saveMock = sandbox.stub(Queue.prototype, 'save', function(){
                return utils.makeObjectPromise({});
            });

            var messageCountMock = sandbox.stub(Message, 'depth', function(queueId) {
                return utils.makeObjectPromise(3);
            });
            var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function(queue) {
                return utils.makeObjectPromise([{consumer: '1'}, {consumer: '2'}]);
                // return ;
            });
            var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
                return utils.makeObjectPromise(putMessage);
            });

            var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
                function (consumers, messageObj) {
                    expect(consumers).to.be.defined;
                    expect(consumers.length).to.equal(2);
                });

            let req = {
                params: {name: 'TestQueue'},
                body: {},
                headers: {'Content-Type': 'application/json'},
                header: function (key) {
                    return this.headers[key];
                }
            };
            let res = {
                headers: {},
                setHeader: function (header, value) {
                    this.headers[header] = value;
                },
                json: function (message) {
                    expect(message.message).to.be.defined;
                    return this;
                },
                send: function () {
                },
                status: function () {
                }
            };

            let statusMock = sandbox.stub(res, 'status', function (code) {
                expect(code).to.equal(400);
                return this;
            });

            yield messageRoutes.postMessageToQueue(req, res);
            sinon.assert.notCalled(consumerLoadAllForQueueMock);
            sinon.assert.notCalled(messagePutMock);
            sinon.assert.notCalled(utilsBroadcastMessageMock);
            sinon.assert.calledOnce(statusMock);
            sinon.assert.notCalled(saveMock);

    });

    it('should return 401 when POSTing a message to a secure queue without providing a valid token', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.token, token:'FAKETOKEN'};
        let loadMock = sandbox.stub(Queue, 'load', function(name) {
            return utils.makeObjectPromise(queue);
        });

        var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
            return utils.makeObjectPromise(putMessage);
        });


        let req = {
            params: {name: 'TestQueue'},
            body: {message:'blah'},
            headers: {'Content-Type': 'application/json'},
            header: function (key) {
                return this.headers[key];
            }
        };
        let res = {
            headers: {},
            setHeader: function (header, value) {
                this.headers[header] = value;
            },
            json: function (message) {
                expect(message.message).to.be.defined;
                return this;
            },
            send: function () {
            },
            status: function () {
            }
        };

        let statusMock = sandbox.stub(res, 'status', function (code) {
            expect(code).to.equal(401);
            return this;
        });

        yield messageRoutes.postMessageToQueue(req, res);

        sinon.assert.calledOnce(statusMock);
        sinon.assert.notCalled(messagePutMock);

    });

    it('should return 500 when a random error occurs when POSTing a message to a queue', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        let loadMock = sandbox.stub(Queue, 'load', function(name) {
            return utils.makeObjectPromise(queue);
        });

        var messagePutMock = sandbox.stub(Message, 'put', function(putMessage) {
            throw {error:'SomeError', message:'Just a random error here..nothing to see..'};
        });


        let req = {
            params: {name: 'TestQueue'},
            body: {message:'blah'},
            headers: {'Content-Type': 'application/json'},
            header: function (key) {
                return this.headers[key];
            }
        };
        let res = {
            headers: {},
            setHeader: function (header, value) {
                this.headers[header] = value;
            },
            json: function (message) {},
            send: function () {},
            status: function () {
            }
        };

        let statusMock = sandbox.stub(res, 'status', function (code) {
            expect(code).to.equal(500);
            return this;
        });

        let sendMock = sandbox.stub(res, 'send', function(message){
            expect(message.message).to.be.defined;
            return this;
        })

        yield messageRoutes.postMessageToQueue(req, res);

        sinon.assert.calledOnce(statusMock);
        sinon.assert.calledOnce(sendMock);

    });

    it('should return a 200 when POSTing a message to an existing queue and the broadcast blows up', function* (){
        // stub load function
        let queue = {name: 'TestQueue', security: config.queueSecurity.none};
        let loadMock = sandbox.stub(Queue, 'load', function(name) {
            return utils.makeObjectPromise(queue);
        });
        let saveMock = sandbox.stub(Queue.prototype, 'save', function(){
            return utils.makeObjectPromise({});
        });

        var messageCountMock = sandbox.stub(Message, 'depth', function(queueId) {
            return utils.makeObjectPromise(3);
        });
        var consumerLoadAllForQueueMock = sandbox.stub(Consumer, 'loadAllForQueue', function(queue) {
            return utils.makeObjectPromise([{consumer: '1'}, {consumer: '2'}]);
            // return ;
        });
        var messagePutMock = sandbox.stub(Message, 'put', function*(putMessage) {
            return utils.makeObjectPromise(putMessage);
        });

        var utilsBroadcastMessageMock = sandbox.stub(utils, 'broadcastMessage',
            function (consumers, messageObj) {
                expect(consumers).to.be.defined;
                expect(consumers.length).to.equal(2);
                throw {error: 'oh no!', message: 'the broadcast failed for an unknown reason!'};
            });

        let req = {
            params: {name: 'TestQueue'},
            body: {message:'some stuff'},
            headers: {'Content-Type': 'application/json'},
            header: function (key) {
                return this.headers[key];
            }
        };
        let res = {
            headers: {},
            setHeader: function (header, value) {
                this.headers[header] = value;
            },
            json: function (message) {
                expect(message.message).to.be.defined;
                return this;
            },
            send: function () {
            },
            status: function () {
            }
        };

        let statusMock = sandbox.stub(res, 'status', function (code) {
            expect(code).to.equal(400);
            return this;
        });

        yield messageRoutes.postMessageToQueue(req, res);
        sinon.assert.calledOnce(consumerLoadAllForQueueMock);
        sinon.assert.calledOnce(messagePutMock);
        sinon.assert.calledOnce(utilsBroadcastMessageMock);
        sinon.assert.notCalled(saveMock);

    });
});