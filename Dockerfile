FROM node:boron

# setup parcel directory
RUN mkdir -p /usr/src/parcel
WORKDIR /usr/src/parcel

# install dependencies
COPY package.json /usr/src/parcel/
RUN npm install --production

COPY . /usr/src/parcel/

# expose the port
EXPOSE 8891

CMD ["npm","start"]