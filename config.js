/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

function Config(){
    this.port = 8891;
    this.secret = '1L0V3secrets@n1ght';
    this.headerPrefix = 'parcel';
    this.messageHeaderPrefix = this.headerPrefix+'-message';
    this.messageExpiryHeader = this.messageHeaderPrefix+'-expiry';
    this.queueHeaderPrefix = this.headerPrefix+'-queue';
    this.queueMethodHeader = this.queueHeaderPrefix+'-method';
    this.queueBrowseHeader = this.queueHeaderPrefix+'-browse';

    // daisy-chaining
    this.parcelForwardHeader = this.headerPrefix+'-forwards';
    this.parcelMaxForwards = 3;
    this.broadcastTimeout = 20;


    this.queueSecurity = {
        none: 'none',
        token: 'token',
        entitled: 'entitled'
    };

    this.queueMethod = {
        FIFO: "FIFO",
        LIFO: "LIFO"
    };

    this.errors = {
        queueRequired: {
            name: 'QueueRequired',
            message: 'A valid queue must be specified',
            code: 400
        },
        subscriptionExists: {
            name: 'SubscriptionExists',
            message: 'A subscription with that name already exists',
            code: 409
        },
        consumerExists: {
            name: 'ConsumerExists',
            message: 'A consumer with that name already exists',
            code: 409
        },
        messageRequired: {
            name: 'MessageRequired',
            message: 'Message must have content',
            code: 400
        },
        tokenInvalid: {
            name: 'TokenInvalid',
            message: 'Must provide a valid token',
            code: 401
        },
        queueNotFound: {
            name: 'QueueNotFound',
            message: 'No queue found with the specified name',
            code: 404
        }

    };
}

module.exports = new Config();