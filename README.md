# Parcel [![build status](https://gitlab.com/tocodev/parcel/badges/master/build.svg)](https://gitlab.com/tocodev/parcel/commits/master) [![coverage report](https://gitlab.com/tocodev/parcel/badges/master/coverage.svg)](https://gitlab.com/tocodev/parcel/commits/master)

Parcel is lightweight, RESTful, persistent message queueing with notification 
via asynchronous web service callbacks.  
Send a message from your server 
into a parcel queue through a REST service call.  If desired, parcel will 
notify registered endpoints that a new message landed in the queue.  
Use another parcel REST service to pull the message from the queue.  You can
set message expiration, control access through tokens and manage your queues 
all through parcel's lightweight and intuitive REST service API.



Create a default queue by posting to the queues collection, or pass your queue    
spec for a more complicated queue setup.    
Lock down access to queues using tokens or use open queues for non-confidential    
data.
## Endpoints
`/api/queues` supports the following methods:
- `GET` get a list of all queues, returns:
    - `200 OK` and a list of all queues (limited to name and security)  
    - `204 No Content` indicating no queues exist  
- `POST` create a queue  
    - `201 Created` and JSON body of the created queue spec
    - `400 Bad Request` indicating that the spec was invalid
    - `409 Conflict` indicating that a queue with that name already exists
    - `415 Unsupported Media Type` currently only application/json is supported
- `PUT`,`PATCH`  
    - `404 Not Found` currently unsupported
- `PATCH`  
    - `404 Not Found` currently unsupported

`/api/queues/{name}`
- `GET ` get a specific queue, returns:
    - `200 OK` and the queue spec
    - `401 Unauthorized` if the queue defined security and the provided token did not match
    - `404 Not Found` if no queue spec exists with the given name
- `POST` Not supported
    - `404 Not Found` not plans for support
- `PUT` Update the specified queue
    - `200 OK` currently unsupported - the queue was updated with the given spec
    - `404 Not Found` currently unsupported - the specified queue was not found
- `PATCH`
    - `404 Not Found` currently unsupported
- `DELETE`  
    - `200 OK` Queue with matching name deleted
    - `204 No Content` indicating no queues with matching name deleted
  
`/api/queues/{name}/messages`  
- `GET`  
    - `200 OK` and the first message on the queue  
    - `204 No Content` indicating the queue is empty  
    - `401 Unauthorized` if queue spec defines security and no/invalid token provided  
    - `404 Not Found` if the queue does not exist    
- `POST`  
    - `200 OK` and the message that was added  
    - `401 Unauthorized` if the queue defined security and the provided token did not match  
    - `415 Unsupported Media Type` currently only application/json is supported  
- `PUT`,`PATCH`
    - `404 Not Found` currently unsupported

# Startup
Run `npm install` from the parcel folder to install dependencies.  Start your
MongoDB instance and configure (if necessary) to point to your
instance of MongoDB.  


# Quick start
To get started, first create a queue (assuming port 8891):   
POST `localhost:8891/api/queues`   
BODY (application/json):   
`{
    "name": "MyQueue",
    "security": "none",
    "token": null,
    "parcelMessage": false,
    "method": "FIFO"
}`
   
Now, query for messages in this new queue (there should be none, obviously):   
GET `localhost:8891/api/queues/MyQueue/messages`   
   
You should get a `204` response (no content).  Let's POST a message to the queue:   
POST `localhost:8891/api/queues/MyQueue/messages`
BODY (application/json):
`{"message":"Hello World!"}`
   
Now, let's query for messages again:   
GET `localhost:8891/api/queues/MyQueue/messages`   
   
We should get a `200` and see the message we added.  Run the query again, and we now get a `204` status.  What happened?  
The `GET` is destructive and will pop the message from the queue.  We can also do a non-destructive `GET`, or a 
"browse" by specifying a URL parameter.  To see this, let's POST again a new message:   
   
POST `localhost:8891/api/queues/MyQueue/messages`   
BODY (application/json):   
`{"message":"Hello World ... again!"}`   
   
Now, if we query for messages again with URL parameter "browse" set to "true", we should be able to `GET` the message without removing it from the queue:   
GET `localhost:8891/api/queues/MyQueue/messages?browse=true`   
   
Try several times, and you will see the message does not leave the queue.   
   
## For connecting to a secure mongo database:  
MONGO_HOST=&lt;host&gt;:&lt;port&gt; MONGO_USER=&lt;user&gt; MONGO_PASS=&lt;password&gt; node server.js  
## For connecting to an open mongo database:  
MONGO_HOST=&lt;host&gt;:&lt;port&gt; node server.js  
## For connecting to default localhost:27017:  
node server.js  
  
#License  
Parcel is an open-source project licensed under GPL v3.
We want an improvements to Parcel to be shared with the community.
Check out more on the license here.