/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const config = require('./config');
const request = require('request');

function Utils(){
    this.isParcelError = function(ex){
        let errors = Object.keys(config.errors);

        for(let error of errors)
            if(config.errors[error] === ex)
                return true;

        return false;
    }

    this.extractHeaders = function(httpHeaders){
        let headers = {};
        for(let header in httpHeaders){
            if(header.startsWith(config.headerPrefix))
                headers[header] = httpHeaders[header];
        }

        return headers;
    }

    this.processMongooseError = function(ex, defaultError){
        let err = {
            code: 500,
            errorBody: {message: 'An error occurred on the server'}
        };

        let retErr = defaultError ? defaultError : err;

        try {
            // TODO: this breaks on a model validate enum
            // Probably a better way to do this ....
            var errorType = ex.errors.name ? (ex.errors.name.properties ? ex.errors.name.properties.type : null) : null;
            if (errorType == "unique") {
                retErr = {
                    code: 409,
                    errorBody: {
                        message: ex.errors.name.message,
                        error: ex
                    }
                };
            } else if (errorType == "required") {
                retErr = {
                    code: 400,
                    errorBody: {
                        message: ex.errors.name.message,
                        error: ex
                    }
                };
            } else if (defaultError) {
                retErr = defaultError;
            }
        }catch(ex2){
            console.error(ex2);
            retErr = defaultError ? defaultError : err;;
        }

        return retErr;
    }

    this.broadcastMessage = function(consumers, message){
        if(!consumers || consumers.length == 0)
            return;

        let forwards = message.headers ? message.headers[config.parcelForwardHeader] : null;
        forwards = forwards ? parseInt(forwards)+1 : 1;

        // Check for & update forwards
        // if(forwards){
        //     forwards++;
        // } else {
        //     forwards = 1;
        // }

        console.log('forwards: '+forwards);
        console.log('maxForwards: '+config.parcelMaxForwards);

        if(forwards <= config.parcelMaxForwards || config.parcelMaxForwards<=0 || !config.parcelMaxForwards){
            if(!message.headers) message.headers = {};
            message.headers[config.parcelForwardHeader] = forwards;
            for(let consumer of consumers)
                if(consumer.subscription)
                    this.sendMessage(consumer.subscription.url, message);
        } else {
            console.warn('A message was not forwarded for being over the parcel-forwards limit of '+config.parcelMaxForwards);
        }
    }

    this.sendMessage = function(endpoint, message){
        let timeout = config.broadcastTimeout ? config.broadcastTimeout*1000 : 30000;

        request.post({
            url: endpoint,
            headers: message.headers,
            json: true,
            body: message.payload,
            timeout: timeout
        }, function(err, response){
            if(err){
                console.error(err);
                return;
            }

            let status = response.statusCode;

            if(status === 200 || status === 201 || status === 202){
                console.log('Message delivered to endpoint ['+endpoint+'] with status code ['+status+']');
            } else {
                console.error('Endpoint ['+endpoint+'] ended in error ['+status+']');
            }
        });
    }

    this.makeObjectPromise = function(obj){
        return new Promise(function (fulfill){fulfill(obj);});
    }
};

module.exports = new Utils();