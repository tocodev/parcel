/*

    This document is part of the source code and related artifacts
    for Parcel, an open source RESTful messaging project.
    Copyright (C) 2017  (TocoDev LLC)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
const config = require('../config');
const jwt = require('jsonwebtoken');
const moment = require('moment');
var Schema = mongoose.Schema;

/*
{
    name: name of the consumer [required],
    token: consumer token [nullable],
    queueId: queue being consumed [required],
    createdDate: date of creation [required],
    lastModifiedDate: date last modified [required],
    subscription: {
        url - subscription url
        queryParams: {map of query param names and values},
        headers: {map of header names and values}
    } [nullable],
    filters: [list of refs to filters]
}
*/

// <string> | <string[]>
var Subscription = new Schema({
    url: {type: Schema.Types.String, required: true},
    queryParams: {
        name: {type: Schema.Types.String},
        value: {type: Schema.Types.String}
    },
    headers: {
        name: {type: Schema.Types.String},
        value: {type: Schema.Types.String}
    }
});

var Consumer = new Schema({
    name: {type: Schema.Types.String, required: true},
    token: {type: Schema.Types.String},
    queueId: {type: Schema.Types.ObjectId, required: true},
    createdDate: {type: Schema.Types.Date, default: moment(), required:true},
    lastModifiedDate: {type: Schema.Types.Date, default: moment(), required:true},
    subscription: {type: Subscription}
    // filters: [type: Filter, ref='Filter']
});



Consumer.statics.loadAllForQueue = function (queue){
    return this.find({queueId: queue._id});
}

Consumer.statics.consume = function* (queue, consumer){
    if(!queue)
        throw config.errors.queueRequired;

    let existing = yield this.findOne({name:consumer.name, queueId: queue._id});

    if(existing){
        console.log('found existing!!');
        console.log(existing);
        throw config.errors.subscriptionExists;
    }

    let now = moment().toDate();

    if(queue.security === 'token'){
        let payload = {
            role: 'consumer',
            queue: queue.name,
            createdDate: now
        };
        let token = jwt.sign(payload, config.secret);
        consumer.token = token;
    }

    consumer.queueId = queue.id;
    consumer.createdDate = now;
    consumer.lastModifiedDate = now;

    console.log('before save');

    consumer = yield consumer.save();

    return consumer;
}

Consumer.statics.regurgitate = function* (queue, consumerName){
    if(!queue)
        throw config.errors.queueRequired;

    let consumer = yield this.findOneAndRemove({name:consumerName, queueId: queue._id});

    return consumer;
}

module.exports = mongoose.model('Consumer', Consumer);




