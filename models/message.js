/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
const config = require('../config');
var Schema = mongoose.Schema;
var moment = require('moment');

var message = new Schema({
    queueId: {type: Schema.Types.ObjectId, required: true},
    headers: {type: Schema.Types.Mixed},
    payload: {type: Schema.Types.Mixed},
    createdDate: {type: Schema.Types.Date, default: moment()},
    expiry: {type: Schema.Types.Number},
    expireDate: {type: Schema.Types.Date}
});

message.statics.browse = function(queueId, method){
    sortDirection = (method && method==config.queueMethod.LIFO?-1:1);
    return this.findOne({queueId:queueId}).sort({createdDate: sortDirection});
};

message.statics.get = function(queueId, method){
    sortDirection = (method && method==config.queueMethod.LIFO?-1:1);
    return this.findOneAndRemove({queueId:queueId}).sort({createdDate: sortDirection});
};

message.statics.put = function(messageObj){
    messageObj.save();
    return messageObj;
};

message.statics.depth = function(queueId){
    return this.count({queueId:queueId});
};

message.statics.clean = function(queueId){
    let now = moment().toDate();
    return this.remove({queueId: queueId, expireDate: {$lte:now}});
};

module.exports = mongoose.model('Message', message);
