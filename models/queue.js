/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config');
const moment = require('moment');
var mongooseUnVal = require('mongoose-unique-validator');
var Message = require('./message');
var Schema = mongoose.Schema;

var queue = new Schema({
    name: {type: String, required: true, unique: true},
    security: {type: String, default: config.queueSecurity.none,
        enum: [config.queueSecurity.none, config.queueSecurity.token, config.queueSecurity.entitled]},
    token: {type: String},
    parcelMessage: {type: Boolean, required: true, default: false},
    method: {type: String, required: true, default: config.queueMethod.FIFO, 
        enum: [config.queueMethod.FIFO, config.queueMethod.LIFO]}
});

queue.methods.generateToken = function(cb){
    let payload = {
        role: 'admin',
        queue: this.name,
        created: moment().toDate()
    };

    console.log('generate');
    let token = jwt.sign(payload, config.secret);
    this.token = token;
}

queue.methods.validateToken = function(token){
}

queue.statics.load = function(name, cb){
    return this.findOne({name:name});
};

queue.statics.delete = function* (queueName){
    return this.findOneAndRemove({name: queueName});
};



queue.plugin(mongooseUnVal);
module.exports = mongoose.model('Queue', queue);