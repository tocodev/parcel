/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

const config = require('./config');
const mongoose = require('mongoose');

let user = process.env.MONGO_USER;
let pass = process.env.MONGO_PASS;
let host = process.env.MONGO_HOST || 'localhost:27017';

mongoose.Promise = global.Promise;

if(user && pass)
    mongoose.connect(host+'/parcel', {user: user, pass: pass});
else
    mongoose.connect(host+'/parcel');

const app = require('./app');

let activePort = process.env.port || config.port;
app.listen(activePort);
console.log('Server started on port '+activePort);
