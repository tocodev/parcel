/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var Queue = require('../models/queue');
var utils = require('../utils');
var config = require('../config');

module.exports = {

    // GET /queues
    getAllQueues: function* (req, res){
        try{
            let queues = yield Queue.find({});
            if(queues) {
                // limit data returned on mass GET
                queues = queues.map(queue => {
                    return {name: queue.name,
                        security: queue.security,
                        method: queue.method}
                });
                return res.json(queues);
            } else
                return res.status(204).send();

        } catch (ex) {
            console.error(ex);
            return res.status(500).send({
                message: 'An error occurred while pulling queues',
                error: ex
            });
        }
    },

    // GET /queues/:name
    getQueueByName: function* (req, res){
        let queueName = req.params.name;
        let token = req.headers.token;

        try{
            let queue = yield Queue.load(queueName);

            console.log('queue found: '+queue)

            if (!queue)
                return res.status(404).send({message: 'Queue not found'});

            if (queue.security !== 'none' && (queue.token !== token))
                return res.status(401).json({message: 'Invalid token'});

            // yield queue.clean();

            return res.json(queue);
        } catch(ex) {
            console.error(ex);
            return res.status(500).send({
                message: 'An error occurred while pulling single queue',
                error: ex
            });
        }
    },

    // POST /queues
    postQueue: function* (req, res){
        // Make sure they sent JSON only as payload
        let contentType = req.header('Content-Type');
        if(!contentType || contentType!="application/json")
            return res.status(415).json({message: 'Request body must be application/json Content-type'});

        console.log(req.body);
        let queueSpec = req.body;

        let queue = new Queue({
            name: queueSpec.name,
            security: queueSpec.security,
            method: queueSpec.method
        });

        console.log('generate token')
        if (queue.security !== 'none') {
            queue.generateToken();
            console.log('token generated');
        }

        try {
            yield queue.save();
        } catch (ex) {
            console.error(ex);

            let defaultError = {code:500, errorBody: {
                message: 'Could not save queue',
                error: ex
            }};
            let err = utils.processMongooseError(ex, defaultError);
            return res.status(err.code).json(err.errorBody);
        }

        return res.status(201).json(queue);
    },

    // DELETE /queues/:name
    deleteQueueByName: function* (req, res){
        let queueName = req.params.name;
        try{
            let result = yield Queue.delete(queueName);

            if(!result)
                res.status(204).send();
            else
                res.status(200).json(result);
        } catch(ex){
            console.error(ex);
            return res.status(500).send({
                message: 'An error occurred while deleting queue ' + queueName,
                error: ex
            });
        }
    }
}