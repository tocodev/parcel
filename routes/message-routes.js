/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var Queue = require('../models/queue');
var Consumer = require('../models/consumer');
const moment = require('moment');
var wrap = require('co-express');
var config = require('../config');
var Message = require('../models/message');
var Consumer = require('../models/consumer');
var utils = require('../utils');
var empty = require('is-empty');

module.exports = {

    // GET /queue/:name/messages
    getMessageFromQueue: function* (req, res){
        let queueName = req.params.name;

        try {
            // Grab the queue within which to put message
            let queue = yield Queue.load(queueName);
            if (!queue)
                return res.status(404).json({
                    message: 'Queue not found'
                });

            yield Message.clean(queue._id);

            // Grab headers if there
            let browseHeader = undefined;
            let methodHeader = undefined;
            if(req.query) {
                browseHeader = req.query.browse;
                methodHeader = req.query.method;
            }
            if(!methodHeader)
                methodHeader = queue.method;

            // Ensure browse and method are correct format
            if(browseHeader && !(browseHeader=='true'||browseHeader=='false'))
                return res.status(400).json({message: 'browse must be true or false'});
            if(methodHeader && !(methodHeader=='FIFO'||methodHeader=='LIFO'))
                return res.status(400).json({message: 'method must be FIFO or LIFO'});

            // Set response headers for browse and method
            res.setHeader(config.queueMethodHeader, methodHeader);
            if(browseHeader) {
                res.setHeader(config.queueBrowseHeader, browseHeader);
            }

            // Grab the message as directed by caller
            let message = (browseHeader && browseHeader == "true")?
                yield Message.browse(queue._id, methodHeader):
                yield Message.get(queue._id, methodHeader);

            // Set the queue depth
            let queueDepth = yield Message.depth(queue._id);
            res.setHeader('Queue-Depth', queueDepth);

            if(message) {
                let payload = null;
                if(queue.parcelMessage){
                    payload = {
                        headers: message.headers,
                        payload: message.payload,
                        createdDate: message.createdDate
                    };

                } else {
                    if(message.headers)
                        for(let header in message.headers)
                            res.setHeader(header,message.headers[header]);
                    payload = message.payload;
                }
                res.json(payload);
            }
            else
                res.status(204).send();
        } catch (ex) {
            console.error(ex);
            return res.status(500).send({
                message: 'An error occurred while pulling queues',
                error: ex
            });
        }
    },

    // POST /queues/:name/messages
    postMessageToQueue: function* (req, res){
        let queueName = req.params.name;
        let message = req.body;
        let token = req.header('token');

        // Make sure they sent JSON only as payload
        let contentType = req.header('Content-Type');
        if(!contentType || contentType!="application/json")
            return res.status(415).json({message: 'Payload must be application/json Content-type'});

        try {
            // Grab the queue within which to put message
            let queue = yield Queue.load(queueName);

            if(!message || empty(message))
                throw config.errors.messageRequired;
            else if(!queue){
                queue = new Queue({name: queueName});
                yield queue.save();
            } else if (queue.security !== 'none' && (queue.token !== token))
                throw config.errors.tokenInvalid;


            yield Message.clean(queue._id);

            let messageObj = null;

            let created = moment();

            if(queue.parcelMessage){
                let headers = message.headers ? message.headers : {};
                messageObj = new Message({
                    headers: headers,
                    payload: message.payload,
                    queueId: queue._id,
                    createdDate: created.toDate()
                });
            }else{
                let headers = utils.extractHeaders(req.headers);
                messageObj = new Message({queueId:queue._id, payload:message,
                    headers: headers, createdDate: created.toDate()});
            }

            // setup expiry and expireDate
            if(messageObj.headers && messageObj.headers[config.messageExpiryHeader]){
                messageObj.expiry = messageObj.headers[config.messageExpiryHeader];
                messageObj.expireDate = created.add(messageObj.expiry, 's').toDate();
            }

            Message.put(messageObj);

            let queueDepth = yield Message.depth(queue._id);
            res.setHeader('Queue-Depth', queueDepth);
            res.json(message).send();

            try{
                let consumers = yield Consumer.loadAllForQueue(queue);
                messageObj.headers['Queue-Depth']=queueDepth;
                console.log('found consumers: '+consumers.length);
                utils.broadcastMessage(consumers, messageObj);
            } catch(ex){
                console.error('error broadcasting to subscribers on queue');
                console.error(ex);
            }

        } catch(ex) {
            console.error('error:' +ex);
            if(utils.isParcelError(ex))
                return res.status(ex.code).json({message: ex.message});
            else
                res.status(500).send({
                    message: 'An error occurred while pulling queues',
                    error: ex
                });
        }
    }
};