/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var express = require('express');
var router = express.Router();
var wrap = require('co-express');
var config = require('../config');
var messageRoutes = require('./message-routes');

router.post('/:name/messages', wrap(function* (req, res) {
    return yield messageRoutes.postMessageToQueue(req, res);
}));

router.get('/:name/messages', wrap(function* (req, res) {
    return yield messageRoutes.getMessageFromQueue(req, res);
}));

module.exports = router;