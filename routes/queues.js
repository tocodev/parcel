/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var express = require('express');
var Queue = require('../models/queue');
var router = express.Router();
const moment = require('moment');
var wrap = require('co-express');
var config = require('../config');
var utils = require('../utils');
var queueRoutes = require('./queue-routes');

router.get('', wrap(function* (req, res) {
    return yield queueRoutes.getAllQueues(req, res);
}));

router.get('/:name', wrap(function* (req, res) {
    return yield queueRoutes.getQueueByName(req, res);
}));

router.post('', wrap(function* (req, res) {
    return yield queueRoutes.postQueue(req, res);
}));

router.delete('/:name', wrap(function* (req, res){
    return yield queueRoutes.deleteQueueByName(req, res);
}));





module.exports = router;