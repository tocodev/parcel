/*

 This document is part of the source code and related artifacts
 for Parcel, an open source RESTful messaging project.
 Copyright (C) 2017  (TocoDev LLC)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Contact us at info@tocodev.com.

 */

var Queue = require('../models/queue');
const moment = require('moment');
var wrap = require('co-express');
var config = require('../config');
var Message = require('../models/message');
var Consumer = require('../models/consumer');
var utils = require('../utils');
var empty = require('is-empty');

module.exports = {

    // GET /queue/:name/consumers
    getConsumersForQueue: function*(req, res) {
        let queueName = req.params.name;
        let token = req.header('token');

        try{
            let queue = yield Queue.load(queueName);

            if(!queue)
                throw config.errors.queueNotFound;
            else if (queue.security !== 'none' && (queue.token !== token))
                throw config.errors.tokenInvalid;

            let consumers = yield Consumer.loadAllForQueue(queue);


            if(!consumers || consumers.length == 0)
                return res.status(204).send();
            else
                return res.json(consumers);

        }catch(ex){
            console.error(ex);
            if(utils.isParcelError(ex))
                return res.status(ex.code).json({message: ex.message});
            else {
                let defaultError = {
                    code: 500,
                    errorBody: {
                        message: 'An error occurred while finding consumers',
                        error: ex
                    }
                };
                let err = utils.processMongooseError(ex, defaultError);
                return res.status(err.code).json(err.errorBody);
            }
        }

    },

    postConsumerOfQueue: function* (req, res){
        let queueName = req.params.name;
        let consumer = req.body;
        let token = req.header('token');

        let contentType = req.header('Content-Type');
        if(!contentType || contentType!="application/json")
            return res.status(415).json({message: 'Request body must be application/json Content-type'});

        try{
            let queue = yield Queue.load(queueName);

            if(!queue)
                throw config.errors.queueNotFound;
            else if (queue.security !== 'none' && (queue.token !== token))
                throw config.errors.tokenInvalid;


            consumer = new Consumer(consumer);

            consumer = yield Consumer.consume(queue, consumer);

            return res.json(consumer);
        } catch(ex){
            console.error(ex);
            if(utils.isParcelError(ex))
                return res.status(ex.code).json({message: ex.message});
            else{
                let defaultError = {code:500, errorBody: {
                    message: 'An error occurred while pulling queues',
                    error: ex
                }};
                let err = utils.processMongooseError(ex, defaultError);
                return res.status(err.code).send(err.errorBody);
            }
        }
    },

    deleteConsumerOfQueue: function* (req, res){
        console.log(req.params);

        let queueName = req.params.name;
        let consumerName = req.params.conname;

        console.log(consumerName);

        try{
            let queue = yield Queue.load(queueName);

            if(!queue)
                return res.status(404).json({
                    message: 'Queue not found'
                });

            // yield queue.clean();

            let consumer = yield Consumer.regurgitate(queue, consumerName);

            if(!consumer)
                return res.status(204).send();
            else
                return res.json(consumer);
        } catch(ex) {
            console.error(ex);

            if(utils.isParcelError(ex))
                return res.status(ex.code).json({message: ex.message});
            else
                return res.status(500).send({
                    message: 'An error occurred while pulling queues',
                    error: ex
                });
        }
    }

}